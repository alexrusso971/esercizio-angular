import { Component } from '@angular/core';

interface NomeCompleto {
  nome: string;
  cognome: string;
}

interface DataDiNascita {
  giorno: number;
  mese: number;
  anno: number;
}

interface Persona {
  nomeCompleto: NomeCompleto;
  dataDiNascita: DataDiNascita;
}

@Component({
  selector: 'app-root',
  templateUrl:'./app.component.html'
})
export class AppComponent {
  persona: Persona = {
    nomeCompleto: {
      nome: 'Alessandro',
      cognome: 'Russo',
    },
    dataDiNascita: {
      giorno: 7,
      mese: 11,
      anno: 1997,
    },
  };
}